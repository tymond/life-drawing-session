# BBD's Krita Script Starter Feb 2018

from PyQt5.QtCore import (Qt, QRect, QPointF, QTimer, pyqtSlot, QObject, pyqtSignal)

class LifeDrawingSessionTimingConfiguration():
    
    durationsList = []
    uniformDuration = 1000
    numberOfDurations = 0
    timingType = "uniform"
    
    # consts
    timingTypeUniform = "uniform"
    timingTypeClass = "class" # not uniform; custom; uses the durations list
    
    valid = False
    
    def __init__(self):
        pass
    
    def initClass(self, durationsList):
        if len(durationsList) <= 0:
            print("Wrong durations list!")
        self.durationsList = durationsList
        self.timingType = self.timingTypeClass
        valid = True
    
    def initUniform(self, uniformDuration: int, numberOfDurations: int):
        self.uniformDuration = uniformDuration
        self.numberOfDurations = numberOfDurations
        self.timingType = self.timingTypeUniform
        valid = True
    
    
    def isValid(self):
        return self.valid
    
    
    
    
    
class LifeDrawingSessionTimingFactory():
    
    templates = ""
    
    def __init__(self):
        self.templates = LifeDrawingSessionTimingTemplates()
        
    
    def uniform(self, duration, numberOfImages):
        return self._returnUniform(duration, numberOfImages)
    
    def class30min(self):
        return self._returnClass(self.templates.simple30minClass())
        
    def class10min(self):
        return self._returnClass(self.templates.simple10minClass())
    
    def testClass(self):
        return self._returnClass(self.templates.testClass())
        
        
        
    def _returnClass(self, durations):
        response = LifeDrawingSessionTimingConfiguration()
        response.initClass(durations)
        print(durations)
        return response
        
    def _returnUniform(self, duration, numberOfImages):
        response = LifeDrawingSessionTimingConfiguration()
        response.initUniform(duration, numberOfImages)
        return response


class LifeDrawingSessionTimingTemplates():
    
    duration1s = 1000
    duration30s = 30*duration1s
    duration1min = 60*duration1s
    duration5min = 5*duration1min
    duration10min = 10*duration1min
    
    
    
    
    def __init__(self):
        pass

    def simple30minClass(self):
        durationsList = []
        for i in range(10):
            durationsList.append(self.duration30s) # 10*0.5
        for i in range(5):
            durationsList.append(self.duration1min) # 5*1
        
        durationsList.append(self.duration5min) # 2*5
        durationsList.append(self.duration5min)
        
        durationsList.append(self.duration10min) # 1*10
        return durationsList

    def simple10minClass(self):
        durationsList = []
        for i in range(8):
            durationsList.append(self.duration30s) # 8*0.5 = 4
        
        durationsList.append(self.duration1min) # 3*1 = 3
        durationsList.append(self.duration1min) 
        durationsList.append(self.duration1min)
        
        durationsList.append(3*self.duration1min) # 1*3 = 3
        return durationsList


    def testClass(self):
        durationsList = []
        durationsList.append(self.duration1min)
        durationsList.append(self.duration30s/2)
        durationsList.append(self.duration1min)
        durationsList.append(self.duration30s/2)
        return durationsList
