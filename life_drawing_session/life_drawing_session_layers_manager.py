# BBD's Krita Script Starter Feb 2018

from PyQt5.QtCore import (Qt, QRect, QPointF, QTime)

#from . import ui_life_drawing_session

import krita
import math


class LifeDrawingSessionLayersManager(object):

    def __init__(self):
        super().__init__()

    def createNewReferenceItem(self, image, index, timeText):
        self.kritaInstance = krita.Krita.instance()
        doc = Application.activeDocument()
        doc.waitForDone()
        time = QTime.currentTime()
        self._hideCurrentPaintingLayer(doc)
        groupLayer = doc.createGroupLayer("reference " + str(index + 1))
        doc.rootNode().addChildNode(groupLayer, None)
        refLayer = self._createNewReferenceLayer(doc, groupLayer, image, index)
        paintLayer = self._createNewPaintingLayer(doc, groupLayer, refLayer, index)
        doc.waitForDone()
        doc.refreshProjection()
        elapsed = QTime.currentTime().msecsTo(time)
        print(str(elapsed) + "ms = " + str(elapsed/1000) + "s")
        
        
    def createFinishingItem(self):
        self.kritaInstance = krita.Krita.instance()
        doc = Application.activeDocument()
        doc.waitForDone()
        self._hideCurrentPaintingLayer(doc)
        nodesList = self._getListOfRelevantNodes(doc)
        self._createShowcaseLayer(doc, nodesList)
        doc.waitForDone()
        doc.refreshProjection()
        

    def waitForDone(self):
        self.kritaInstance = krita.Krita.instance()
        doc = Application.activeDocument()
        doc.waitForDone()


    def _hideCurrentPaintingLayer(self, doc):
        currentNode = doc.activeNode()
        node = currentNode
        
        if node != None and currentNode.type() != "grouplayer":
            node = currentNode.parentNode()
        
        # last condition because of the bug in Node.h (parentNode is never None)
        if node != None and node.type() == "grouplayer" and node.parentNode() != None and node.name() != "root":
            node.setVisible(0)
            node.setLocked(1)
        
        
    def _createNewReferenceLayer(self, doc, parent, imagePath, index):
        newLayer = doc.createFileLayer("file layer " + str(index + 1), imagePath, "ToImageSize")
        parent.addChildNode(newLayer, None)
        doc.setActiveNode(newLayer)
        self.kritaInstance.action("convert_to_paint_layer").trigger()
        newLayer = doc.activeNode()
        newLayer.setParent(parent)
        print("### reference layers bounds = ")
        print(newLayer.bounds())
        (w, h) = self._calculateReferenceImagesSize(doc, newLayer.bounds())
        print("### reference layers bounds scaled = ")
        print((w, h))
        newLayer.scaleNode(QPointF(0, 0), w, h, "Bicubic")
        #newLayer.move(0, doc.height()/2)
        wallowed = doc.width()
        hallowed = doc.height()
        print("### image size = ")
        print((wallowed, hallowed))
        print("### move to ")
        print(((hallowed - h)/2, 0))
        doc.waitForDone()
        self._moveTo(newLayer, 0, (hallowed - h)/2)
        doc.waitForDone()
        return newLayer
        
    def _calculateReferenceImagesSize(self, doc, refBounds):
        if refBounds.width() == 0 or refBounds.height() == 0:
            return (refBounds.width(), refBounds.height())
        h = doc.height()
        w = doc.width()
        # TODO: option: horizontal or vertical
        # for now, just horizontal
        wallowed = w/2
        hallowed = h
        wscale = wallowed/refBounds.width()
        hscale = hallowed/refBounds.height()
        scale = min(wscale, hscale)
        return (scale*refBounds.width(), scale*refBounds.height())
        
        
        

    def _createNewPaintingLayer(self, doc, parent, above, index):
        newLayer = doc.createNode("paint layer " + str(index + 1), "paintlayer")
        parent.addChildNode(newLayer, None)
        return newLayer
        
        
    def _isPaintingLayer(self, name):
        if name.startswith("paint"):
            return True
        return False
        
    def _getListOfRelevantNodes(self, doc):
        nodesList = []
        root = doc.rootNode()
        return self._getListOfRelevantNodesRecursive(root, nodesList)
        
    def _getListOfRelevantNodesRecursive(self, node, nodesList):
        if node.type() == "paintlayer":
            if self._isPaintingLayer(node.name()):
                nodesList.append(node)
        for child in node.childNodes():
            nodesList = self._getListOfRelevantNodesRecursive(child, nodesList)
        return nodesList
        
        
    def _rasterize(self, doc, node):
        oldActive = doc.activeNode()
        doc.setActiveNode(node)
        self.kritaInstance.action("convert_to_paint_layer").trigger()
        node = doc.activeNode()
        if node.name() != oldActive.name():
            doc.setActiveNode(oldActive)
        return node
        
        
    def _createShowcaseLayer(self, doc, nodesList):
        width = 0
        height = 0
        copies = []
        for node in nodesList:
            width += node.bounds().width()
            height = max(height, node.bounds().height())
            copies.append(node.duplicate())
            
        print("together: " + str(width) + "x" + str(height))
        
        groupLayer = doc.createGroupLayer("showcase group")
        doc.rootNode().addChildNode(groupLayer, None)
        
        groupLayer.setChildNodes(copies)
        doc.refreshProjection()
        
        margin = 1.1
        
        rowsNum, columnsNum, scaleNum = self._findCorrectScaleForShowcase(len(copies), width, height, doc.width(), doc.height())
        
        if scaleNum == 0:
            scaleNum = 1 # to prevent dividing by zero
        
        wnow = 0
        imagei = 0
        imagej = 0
        for node in copies:
            # scale down
            # move to the correct place
            print("old w/h: " + str(node.bounds()))
            wnew = node.bounds().width()/scaleNum
            hnew = node.bounds().height()/scaleNum
            if int(wnew) != 0 and int(hnew) != 0:
                print("new w/h: " + str(wnew) + " " + str(hnew))
                print("new wnow: " + str(wnow))
                node.scaleNode(QPointF(0, 0), wnew, hnew, "Bicubic")
                doc.waitForDone()
                doc.refreshProjection()
            
            # correct place:
            if imagei >= columnsNum:
                imagei = 0
                imagej += 1
                wnow = 0
            
            self._moveTo(node, wnow, imagej*height/scaleNum)
            
            wnow += wnew
            doc.waitForDone()
            doc.refreshProjection()
            imagei += 1
        
        
        doc.waitForDone()    
        doc.refreshProjection()
        
        newLayer = self._rasterize(doc, groupLayer)
        root = doc.rootNode()
        rootchildren = root.childNodes()
        rootlast = rootchildren[len(rootchildren) - 1]
        
        doc.waitForDone()
        doc.refreshProjection()
        
        print("root child layer = " + rootlast.name())
        print("root child layer bounds = " + str(rootlast.bounds()))
        print("position before: " + str(rootlast.position()))
        self._moveTo(rootlast, 0, 0)
        self._centerNode(doc, rootlast)
        print("position after: " + str(rootlast.position()))
        
        doc.waitForDone()    
        doc.refreshProjection()
        
        
        
        
    def _centerNode(self, doc, node):
        self._moveTo(node, doc.width()/2 - node.bounds().width()/2, doc.height()/2 - node.bounds().height()/2)
        
    def _moveTo(self, node, x, y):
        movex = node.position().x() - node.bounds().x() + x
        movey = node.position().y() - node.bounds().y() + y
        print(str(movex) + "|" + str(movey))
        node.move(movex, movey)
        
    
        
    def _findCorrectScaleForShowcase(self, nodesCount, wmax, hmax, w, h):
        ratioOri = wmax/hmax
        ratio = w/h
        # how many rows: very naive implementation
        # base size: x * y
        # base size of one image: x/2 * y (half, horizontally)
        # base size of all images: a*x/2 * y
        rows = math.ceil(math.sqrt((wmax*hmax)/(w*h)))
        columns = math.ceil(nodesCount/rows)
        scale = rows#math.sqrt((wmax*hmax)/(w*h))
        return (rows, columns, scale)
        if ratio > ratioOri:
            # cut in half in 
            ratioOld = ratio
            while ratio > ratioOri:
                rows += 1
                hh = h/rows
                wh = w*rows
                ratio = wh/hh
            if ratioOld - ratioOri < ratio - ratioOri:
                rows -= 1
        # else: 
            # ratioOld = ratio
            # while ratio < ratioOri:
                # rows += 1
                # hh = h*rows
                # wh = w/rows
                # ratio = wh/hh
            # if ratioOld - ratioOri < ratio - ratioOri:
                # rows -= 1
                
        return rows
                
        
        
        
        
        
