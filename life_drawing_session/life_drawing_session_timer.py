# BBD's Krita Script Starter Feb 2018

from PyQt5.QtCore import (Qt, QRect, QPointF, QTimer, pyqtSlot, QObject, pyqtSignal)

#from . import ui_life_drawing_session
from . import life_drawing_session_docker

import krita


class LifeDrawingSessionTimerInstruction():
    imagePath = ""
    time = 0
    
    def __init__(self, imagePath, time):
        self.imagePath = imagePath
        self.time = time
    
    
    


class LifeDrawingSessionTimer(QObject):

    timersInstructionsList = []
    timer = None
    currentInstruction = None
    imageId = 0
    
    layersManager = None
    ui = None


    def __init__(self, layersManager):
        super().__init__()
        self.layersManager = layersManager
        
    def setTimerInstructionList(self, timerInstructionsList):
        self.timersInstructionsList = timerInstructionsList

    def addTimerInstruction(self, image, time):
        ti = LifeDrawingSessionTimerInstruction(image, time)
        self.timersInstructionsList.append(ti)
        print("add " + image + " " + str(time))
        print(self.timersInstructionsList)
        
    def start(self):
        self._startNew()
        
    def _startNew(self):
        self.currentInstruction = self.timersInstructionsList.pop(0)
        
        # we need to ask for update
        ti = self.currentInstruction
        if self.ui != None:
            self.ui.setInfoText(str(self.imageId) + " " + self._timeToUserString(ti.time))
        else:
            print("SORRY, it's NONE")
        
        self.timer = self._createTimer(self.currentInstruction)
        
        
        self.layersManager.createNewReferenceItem(ti.imagePath, self.imageId, "layer " + str(self.imageId) + " " + str(ti.time))
        print(self.currentInstruction.imagePath + ": " + str(self.currentInstruction.time))
        
        

    def _createTimer(self, ti):
        timer = QTimer()
        success = timer.timeout.connect(self._timeout)
        timer.setSingleShot(True)
        timer.start(ti.time)
        return timer
        

    def setUiToUpdateText(self, ui):
        print("SET UI")
        self.ui = ui
        

    @pyqtSlot(name="_timeout")
    def _timeout(self):
        print("TIMEOUT")
        self.imageId += 1 # to make it from 1 
        if len(self.timersInstructionsList) == 0:
            self.layersManager.createFinishingItem()
            self.layersManager.waitForDone()
            return
        self._startNew()
        
    def _timeToUserString(self, time):
        secsall = int(time/1000)
        minsall = int(secsall/60)
        if minsall == 0:
            return str(secsall) + "s"
        else:
            secs = secsall - minsall*60
            if secs == 0:
                return str(minsall) + "min"
            else:
                return str(minsall) + "min " + str(secs) + "s"
        
    
