
from . import life_drawing_session_dialog
from . import life_drawing_session_layers_manager
from . import life_drawing_session_timer
from . import life_drawing_session_images_manager
from . import life_drawing_session_timing_manager



from PyQt5.QtCore import (Qt, QRect, QStandardPaths)
from PyQt5.QtWidgets import (QFormLayout, QListWidget, QHBoxLayout,
                             QDialogButtonBox, QVBoxLayout, QFrame,
                             QPushButton, QAbstractScrollArea, QLineEdit,
                             QMessageBox, QFileDialog, QCheckBox, QSpinBox,
                             QComboBox, QLineEdit, QSizePolicy, QLabel)
import os
import krita


class UILifeDrawingSession(object):


    lds = None
    referenceFolderPath = "./"
    
    classMode30 = 1
    classMode60 = 2
    classMode10 = 3
    
    
    ldsConfigGroup = "LifeDrawingSession"
    

    def __init__(self):
        self.mainDialog = life_drawing_session_dialog.LifeDrawingSessionDialog()
        self.mainLayout = QVBoxLayout(self.mainDialog)
        
        
        self.folderPathLayout = QHBoxLayout(self.mainDialog)
        
        
        self.folderPathTextBox = QLineEdit()
        self.folderPathChooseButton = QPushButton(i18n("Choose..."))
        self.folderPathChooseButton.clicked.connect(self._folderPathChooseButtonClicked)
        
        self.slotDurationCombobox = QComboBox()
        dur1s = 1000
        dur1min = 60*dur1s

        
        testMode = 30
        
        self.slotDurationCombobox.addItem(i18n("30s"), 30*dur1s)
        self.slotDurationCombobox.addItem(i18n("1min"), dur1min)
        self.slotDurationCombobox.addItem(i18n("1.5 min"), 1.5*dur1min)
        self.slotDurationCombobox.addItem(i18n("2min"), 2*dur1min)
        self.slotDurationCombobox.addItem(i18n("3min"), 2*dur1min)
        
        self.slotDurationCombobox.addItem(i18n("5min"), 5*dur1min)
        self.slotDurationCombobox.addItem(i18n("10min"), 10*dur1min)
        self.slotDurationCombobox.addItem(i18n("20min"), 20*dur1min)
        
        
        self.slotDurationCombobox.addItem(i18n("Class Mode 10min"), self.classMode10)
        self.slotDurationCombobox.addItem(i18n("Class Mode 30min"), self.classMode30)
        
        #self.slotDurationCombobox.addItem(i18n("Class Mode Test"), testMode)
        #self.slotDurationCombobox.addItem(i18n("Class Mode 60min"), classMode60)
        
        
        self.slotNumber = QSpinBox()
        self.slotNumber.setMinimum(1)
        self.slotNumber.setMaximum(100)
        self.slotNumber.setValue(5)
        self.slotNumber.setSuffix(" images")
        
        
        self.acceptButton = QPushButton(i18n("OK"))
        self.acceptButton.clicked.connect(self._acceptButtonClicked)
        
        
        
        self.layersManager = life_drawing_session_layers_manager.LifeDrawingSessionLayersManager()
        
        #pictures = #include <QStandardPaths> // QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)
        self.referenceFolderPath = krita.Krita.instance().readSetting(self.ldsConfigGroup, "images_path", 
            QStandardPaths.writableLocation(QStandardPaths.PicturesLocation))
        self.folderPathTextBox.setText(self.referenceFolderPath)
        
        self.imagesNumber = int(krita.Krita.instance().readSetting(self.ldsConfigGroup, "images_number", str(5)))
        self.slotNumber.setValue(self.imagesNumber)
        
        
        
    def initialize(self, lds):
        
        # folder path layout
        self.folderPathLayout.addWidget(self.folderPathTextBox)
        self.folderPathLayout.addWidget(self.folderPathChooseButton)
        
        # main layout
        self.mainLayout.addWidget(QLabel(i18n("Reference Images Folder:")))
        self.mainLayout.addLayout(self.folderPathLayout)
        self.mainLayout.addWidget(self.slotDurationCombobox)
        self.mainLayout.addWidget(self.slotNumber)
        self.mainLayout.addWidget(self.acceptButton)
        
        #self.mainDialog.resize(500, 300)
        self.mainDialog.setWindowTitle(i18n("Life Drawing Session"))
        self.mainDialog.setSizeGripEnabled(True)
        self.mainDialog.show()
        self.mainDialog.activateWindow()
        
        self.lds = lds

    def _acceptButtonClicked(self):
            
        krita.Krita.instance().writeSetting(self.ldsConfigGroup, "images_path", self.referenceFolderPath)
        self.imagesNumber = self.slotNumber.value()
        krita.Krita.instance().writeSetting(self.ldsConfigGroup, "images_number", str(self.imagesNumber))
        
        
        imagesManager = life_drawing_session_images_manager.LifeDrawingSessionImagesManager(self.referenceFolderPath)
        time = self.slotDurationCombobox.currentData(Qt.UserRole)
        timingInstruction = self._getTimingInstruction(time)
                
        durationsList = imagesManager.getDurationsImagesList(timingInstruction)
        
        self.ldsTimer = life_drawing_session_timer.LifeDrawingSessionTimer(self.layersManager)
        self.ldsTimer.setTimerInstructionList(durationsList)
        self.ldsTimer.start()
        self.lds.setLdsTimer(self.ldsTimer)
        self.mainDialog.accept()
        
        
    
    def _folderPathChooseButtonClicked(self):
        self.referenceFolderPath = QFileDialog.getExistingDirectory(self.mainDialog, i18n("Choose Reference Images Directory"), self.referenceFolderPath,
                                                QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks);
        print("chosen path = " + self.referenceFolderPath)
        self.folderPathTextBox.setText(self.referenceFolderPath)
        # saving to settings happens on OK
        # and reading from settings happens on init
        
        
        
        

    def _getTimingInstruction(self, time):
        if time > 100:
                imagesNumber = self.slotNumber.value()
                timingInstruction = life_drawing_session_timing_manager.LifeDrawingSessionTimingFactory().uniform(time, imagesNumber)
        elif time == self.classMode30:
                timingInstruction = life_drawing_session_timing_manager.LifeDrawingSessionTimingFactory().class30min()  
        else:
                timingInstruction = life_drawing_session_timing_manager.LifeDrawingSessionTimingFactory().class10min()
        
        #print(timingInstruction)
        return timingInstruction
        
        
        
            
            
            
