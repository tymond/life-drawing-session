'''

@package life_drawing_session_docker

'''

# Importing the relevant dependencies:
from PyQt5.QtCore import pyqtSlot, Qt, QPointF
from PyQt5.QtGui import (QStandardItem, QStandardItemModel, QPainter, QPalette,
                         QPixmap, QImage, QBrush, QPen, QIcon)
from PyQt5.QtWidgets import QWidget, QTabWidget, QListView, QVBoxLayout, QPushButton, QLabel
from krita import DockWidget


from . import ui_life_drawing_session



class LifeDrawingSessionDocker(DockWidget):
    # Init the docker

    def __init__(self):
        super(LifeDrawingSessionDocker, self).__init__()
        # make base-widget and layout
        widget = QWidget()
        layout = QVBoxLayout()
        widget.setLayout(layout)
        self.setWindowTitle(i18n("Life Drawing Session Docker"))
        
        
        self.infoTextLabel = QLabel();
        self.infoTextLabel.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.infoTextLabel, Qt.AlignCenter)
        
        self.startButton = QPushButton(i18n("Start"))
        self.startButton.clicked.connect(self._startButtonClicked)
        layout.addWidget(self.startButton)
        
        

        self.setWidget(widget)

    def _startButtonClicked(self):
        self.ui = ui_life_drawing_session.UILifeDrawingSession()
        self.ui.initialize(self)
        pass
        
    def __del__(self):
        # body of destructor
        print("### DELETE THE DOCKER!!!")
        
    def canvasChanged(self, canvas):
        pass
    
        
    def setLdsTimer(self, ldsTimer):
        self.sessionTimer = ldsTimer
        ldsTimer.setUiToUpdateText(self)
        
        
    def setInfoText(self, text):
        self.infoTextLabel.setText(text)
        
