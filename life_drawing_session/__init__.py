#from .life_drawing_session import LifeDrawingSession

# And add the extension to Krita's list of extensions:
#app = Krita.instance()
# Instantiate your class:
#extension = LifeDrawingSession(parent=app)
#app.addExtension(extension)


from krita import DockWidgetFactory, DockWidgetFactoryBase
from .life_drawing_session_docker import LifeDrawingSessionDocker


Application.addDockWidgetFactory(
    DockWidgetFactory("life_drawing_session_docker",
                      DockWidgetFactoryBase.DockRight,
                      LifeDrawingSessionDocker))
