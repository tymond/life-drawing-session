# BBD's Krita Script Starter Feb 2018

from PyQt5.QtCore import (Qt, QRect, QPointF, QTimer, pyqtSlot, QObject, pyqtSignal, QDir)

#from . import ui_life_drawing_session
from . import life_drawing_session_timer

import krita
import random


class LifeDrawingSessionImagesManager():
    
    directoryPath = ""
    
    def __init__(self, directoryPath):
        self.directoryPath = directoryPath
        

    def getDurationsList(self, numOfFiles, duration):
        current = QDir(".") # to make sure we provide correct relative paths to FileLayer
        #QDirIterator iterator(self.directoryPath.absolutePath(), QDirIterator.Subdirectories);
        imagesDir = QDir(self.directoryPath)
        #imagesDirPath = current.rela
        files = imagesDir.entryList(QDir.Readable | QDir.Files)
        print(files)
        
        durationsList = []
        for f in files:
            fpath = current.relativeFilePath(self.directoryPath + QDir.separator() + f)
            print("created path = " + fpath)
            timerInfo = life_drawing_session_timer.LifeDrawingSessionTimerInstruction(fpath, duration)
            durationsList.append(timerInfo)
            
        random.shuffle(durationsList)
        num = min(len(durationsList), numOfFiles)
        return durationsList[:num]


    def getDurationsImagesList(self, timingConfiguration):
        current = QDir(".") # to make sure we provide correct relative paths to FileLayer
        imagesDir = QDir(self.directoryPath)
        files = imagesDir.entryList(QDir.Readable | QDir.Files)
        
        filesNumberNeeded = 0
        if timingConfiguration.timingType == timingConfiguration.timingTypeClass:
            filesNumberNeeded = len(timingConfiguration.durationsList)
        else:
            filesNumberNeeded = timingConfiguration.numberOfDurations
        
        filesNumberNeeded = min(filesNumberNeeded, len(files))
        
        selectedFiles = random.choices(files, k=filesNumberNeeded)
        
        i = 0
        durationsList = []
        for f in selectedFiles:
            fpath = current.relativeFilePath(self.directoryPath + QDir.separator() + f)
            
            duration = 1
            if timingConfiguration.timingType == timingConfiguration.timingTypeClass:
                duration = timingConfiguration.durationsList[i]
            else:
                duration = timingConfiguration.uniformDuration
            
            timerInfo = life_drawing_session_timer.LifeDrawingSessionTimerInstruction(fpath, duration)
            durationsList.append(timerInfo)
            i = i+1
        
        return durationsList


